package utils;

import java.util.Arrays;
import java.util.Comparator;

public class LongestWord {

    /**
     * Regex to find whitespace characters, optionally surrounded by punctuation
     */
    private static final String WORD_SEPARATOR_REGEX = "[\\p{Punct}]*[\\s]+[\\p{Punct}]*";

    /**
     * Fetches the longest word for a given string.
     * @param   input  string to search for longest word
     * @return  Result record consisting of the longest word and it's length
     */
    public static Result findLongestWord(final String input) {

        if (input == null) {
            return new Result("");
        }

        final String[] words = input.split(WORD_SEPARATOR_REGEX);

        final String longestWord = Arrays.stream(words)
                .max(Comparator.comparingInt(String::length))
                .orElse("");

        return new Result(longestWord);
    }

    /**
     * Record for the result of findLongestWord
     */
    public static class Result {

        private final String word;
        private final int length;

        public Result(final String word) {
            this.word = word;
            this.length = word.length();
        }

        public String getWord() {
            return word;
        }

        public int getLength() {
            return length;
        }
    }
}
