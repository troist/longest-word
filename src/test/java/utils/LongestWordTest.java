package utils;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class LongestWordTest {

    @Test
    public void longestWord() {
        final String sentence = "The cow jumped over the moon.";
        LongestWord.Result result = LongestWord.findLongestWord(sentence);
        assertEquals("jumped", result.getWord());
        assertEquals(6, result.getLength());
    }

    @Test
    public void emptyString() {
        final String sentence = "";
        LongestWord.Result result = LongestWord.findLongestWord(sentence);
        assertEquals("", result.getWord());
        assertEquals(0, result.getLength());
    }

    @Test
    public void nullInput() {
        LongestWord.Result result = LongestWord.findLongestWord(null);
        assertEquals("", result.getWord());
        assertEquals(0, result.getLength());
    }

    @Test
    public void extraWhitespace() {
        final String sentence = "The \t cow   jumped over\t\t\tthe\tmoon.";
        LongestWord.Result result = LongestWord.findLongestWord(sentence);
        assertEquals("jumped", result.getWord());
        assertEquals(6, result.getLength());
    }

    @Test
    public void ignorePunctuationAtStartOfWord() {
        final String sentence = "John said 'Testing is fun'";
        LongestWord.Result result = LongestWord.findLongestWord(sentence);
        assertEquals("Testing", result.getWord());
        assertEquals(7, result.getLength());
    }

    @Test
    public void ignorePunctuationAtEndOfWord() {
        final String sentence = "Hello, my name is John";
        LongestWord.Result result = LongestWord.findLongestWord(sentence);
        assertEquals("Hello", result.getWord());
        assertEquals(5, result.getLength());
    }

    @Test
    public void wd40() {
        final String sentence = "What does WD-40 do?";
        LongestWord.Result result = LongestWord.findLongestWord(sentence);
        assertEquals("WD-40", result.getWord());
        assertEquals(5, result.getLength());
    }

    @Test
    public void longestWordIsNumber() {
        final String sentence = "Bob has 1000000 apples";
        LongestWord.Result result = LongestWord.findLongestWord(sentence);
        assertEquals("1000000", result.getWord());
        assertEquals(7, result.getLength());
    }

    @Test
    public void multipleLongestWords() {
        final String sentence = "What does this pick";
        LongestWord.Result result = LongestWord.findLongestWord(sentence);

        final List<String> validMatches = Arrays.asList("What", "does", "this", "pick");
        assertTrue(validMatches.contains(result.getWord()));
        assertEquals(4, result.getLength());
    }
}