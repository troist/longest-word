# Longest Word
## Overview
This utility provides a method for finding the longest word in a given string.

## Usage
### Maven
1. Run `mvn clean install` in this project.
2. Import via following dependency:
    ```xml
    <dependency>
        <groupId>utils</groupId>
        <artifactId>longest-word</artifactId>
        <version>1.0</version>
    </dependency>
    ```
3. Use the method `LongestWord.findLongestWord(String input)`
4. This returns a `LongestWord.Result` object, with `getWord()` and `getLength()` methods available.

### Tests
The Maven Surefire plugin will run the JUnit 5 tests available in the [LongestWordTest](src/test/java/utils/LongestWordTest.java) file. This can be triggered with the command `mvn test`.

If hosted on Gitlab, this project will also have JUnit test reports available via Gitlab CI. Go to the [latest Pipeline](https://gitlab.com/troist/longest-word/pipelines/latest) and check under the Tests tab.


## Assumptions
- Punctuation does not count as part of a word if at the start or end of the word.
- Numbers are words.
- Hyphenated words are one word.
- Null and empty strings should return an empty string.